<?php
  class Aeropuerto extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function insertar($datos){
        return $this->db
                ->insert("aeropuerto",
                $datos);
    }
    function obtenerTodos(){
      $listadoAeropuertos=
      $this->db->get("aeropuerto");
      //VALIDACION
      if($listadoAeropuertos
        ->num_rows()>0){//SI hay datos
        return $listadoAeropuertos->result();
      }else{//No hay datos
        return false;
      }
    }

    //FUNCION PARA BORRAR 
    function borrar($id_ar){
      $this->db->where("id_ar",$id_ar);
      return $this->db->delete("aeropuerto");
    }
  }//Cierre de la clase

 ?>
