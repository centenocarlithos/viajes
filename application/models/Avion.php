<?php
  class Avion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function insertar($datos){
        return $this->db
                ->insert("avion",
                $datos);
    }
    function obtenerTodos(){
      $listadoAviones=
      $this->db->get("avion");
      //VALIDACION
      if($listadoAviones
        ->num_rows()>0){//SI hay datos
        return $listadoAviones->result();
      }else{//No hay datos
        return false;
      }
    }

    //FUNCION PARA BORRAR
    function borrar($id_av){
      $this->db->where("id_av",$id_av);
      return $this->db->delete("avion");
    }
  }//Cierre de la clase

 ?>
