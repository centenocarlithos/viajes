<?php
  class Piloto extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function insertar($datos){
        return $this->db
                ->insert("piloto",
                $datos);
    }
    function obtenerTodos(){
      $listadoPilotos=
      $this->db->get("piloto");
      //VALIDACION
      if($listadoPilotos
        ->num_rows()>0){//SI hay datos
        return $listadoPilotos->result();
      }else{//No hay datos
        return false;
      }
    }

    //FUNCION PARA BORRAR
    function borrar($id_pi){
      $this->db->where("id_pi",$id_pi);
      return $this->db->delete("piloto");
    }
  }//Cierre de la clase

 ?>
