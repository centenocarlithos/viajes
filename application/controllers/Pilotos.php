<?php
    class Pilotos extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        $this->load->model('Piloto');
      }

      //FUNCON RENDERIZAR LA VISTA
      public function registrar(){
        $this->load->view('header');
        $this->load->view('pilotos/registrar');
        $this->load->view('footer');
      }

      public function listar(){
        $data['pilotos']=$this->Piloto->obtenerTodos();
        $this->load->view('header');
        $this->load->view('pilotos/listar',$data);
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoPiloto=array(
          "cedula_pi"=>$this->input->post('cedula_pi'),
          "apellidos_pi"=>$this->input->post('apellidos_pi'),
          "nombres_pi"=>$this->input->post('nombres_pi'),
          "telefono_pi"=>$this->input->post('telefono_pi'),
          "edad_pi"=>$this->input->post('edad_pi')
        );
        //imprime los datos del array que creamos
        if ($this->Piloto->insertar($datosNuevoPiloto)) {
          redirect('pilotos/listar');
        }else{
          //embebiendo codigo html dentor de php
          echo "<h1>ERROR INSERTAR</h1>";
        }
      }
      //funcion eliminar instructores
      public function eliminar($id_pi){
        if ($this->Piloto->borrar($id_pi)){//invocando al modelo
          redirect('Pilotos/listar');
        } else {
          echo "ERROR AL BORRAR :(";
        }
      }

    }//CIERRE
 ?>
