<?php
    class Aviones extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        $this->load->model('Avion');
      }

      //FUNCON RENDERIZAR LA VISTA
      public function registrar(){
        $this->load->view('header');
        $this->load->view('aviones/registrar');
        $this->load->view('footer');
      }

      public function listar(){
        $data['aviones']=$this->Avion->obtenerTodos();
        $this->load->view('header');
        $this->load->view(
          'aviones/listar',$data);
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoAvion=array(
          "codigo_av"=>$this->input->post('codigo_av'),
          "modelo_av"=>$this->input->post('modelo_av'),
          "origen_av"=>$this->input->post('origen_av'),
          "destino_av"=>$this->input->post('destino_av'),
          "capacidad_av"=>$this->input->post('capacidad_av')
        );
        //imprime los datos del array que creamos
        if ($this->Avion->insertar($datosNuevoAvion)) {
          redirect('aviones/listar');
        }else{
          //embebiendo codigo html dentor de php
          echo "<h1>ERROR INSERTAR</h1>";
        }
      }
      //funcion eliminar instructores
      public function eliminar($id_av){
        if ($this->Avion->borrar($id_av)){//invocando al modelo
          redirect('Aviones/listar');
        } else {
          echo "ERROR AL BORRAR :(";
        }
      }

    }//CIERRE
 ?>
