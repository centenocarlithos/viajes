<?php
    class Aeropuertos extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        $this->load->model('Aeropuerto');
      }

      //FUNCON RENDERIZAR LA VISTA
      public function registrar(){
        $this->load->view('header');
        $this->load->view('aeropuertos/registrar');
        $this->load->view('footer');
      }

      public function listar(){
        $data['aeropuertos']=$this->Aeropuerto->obtenerTodos();
        $this->load->view('header');
        $this->load->view(
          'aeropuertos/listar',$data);
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoAeropuerto=array(
          "code_ar"=>$this->input->post('code_ar'),
          "ciudad_ar"=>$this->input->post('ciudad_ar'),
          "nombre_ar"=>$this->input->post('nombre_ar'),
          "telefono_ar"=>$this->input->post('telefono_ar'),
          "continente_ar"=>$this->input->post('continente_ar')
        );
        //imprime los datos del array que creamos
        if ($this->Aeropuerto->insertar($datosNuevoAeropuerto)) {
          redirect('aeropuertos/listar');
        }else{
          //embebiendo codigo html dentor de php
          echo "<h1>ERROR INSERTAR</h1>";
        }
      }
      //funcion eliminar instructores
      public function eliminar($id_ar){
        if ($this->Aeropuerto->borrar($id_ar)){//invocando al modelo
          redirect('Aeropuertos/listar');
        } else {
          echo "ERROR AL BORRAR :(";
        }
      }

    }//CIERRE
 ?>
