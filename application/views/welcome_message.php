<div class="container">
	<div class="jumbotron" style="background-image: url('assets/images/inicio.jpeg'); color:black;background-size: cover; background-position: center;">
	  <h2>VIAJES STEAM</h2>
	  <p>Agenda tus viajes ya!</p>
	  <p><a class="btn btn-default btn-lg" href="<?php echo site_url(); ?>/aeropuertos/registrar" role="button">Sucursales</a></p>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
	  <div class="col-xs-6 col-md-3">
	      <img src="<?php echo base_url(); ?>/assets/images/inicio.jpg" alt="imagen1" width="100%"  height="200px">
	  </div>
		<div class="col-xs-6 col-md-3">
	      <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="imagen2" width="100%"  height="200px">
	  </div>
		<div class="col-xs-6 col-md-3">
	      <img src="<?php echo base_url(); ?>/assets/images/3.jpg" alt="imagen3" width="100%"  height="200px">
	  </div>
		<div class="col-xs-6 col-md-3">
	      <img src="<?php echo base_url(); ?>/assets/images/4.webp" alt="imagen4" width="100%"  height="200px">
	  </div>
	</div>
</div>
<br>
<div class="container text-justify">
	<div class="row">
		<div class="col-md-6">
			<h2>MISIÓN</h2>
			<p>La empresa brinda una asistencia profesional a todos aquellos que necesiten un servicio de viaje. El esfuerzo está centrado en brindar respuestas rápidas, precisas y eficientes, buscando satisfacer a nuestros clientes con nuestro servicio, asesoramiento, compromiso, precio, solución de posibles inconvenientes (antes, durante ó después de cada viaje). Damos un servicio a medida, tanto para empresas, como para pasajeros individuales, ya sea viajen por placer o negocios.
				La capacidad de nuestro personal para brindar respuestas adecuadas, forma parte de nuestra fortaleza, haciendo seguimiento permanente sobre las operaciones que se efectúan.
				Nuestra principal herramienta es el acceso directo a la información a través de redes on-line de comunicación, sumado a un conocimiento de los PP indicados para cada servicio, así como condiciones mejoradas en relación a otras Agencias de la zona.
				El manejo adecuado de bases de datos obtenidas de nuestros clientes, hace más dinámica la gestión comercial.</p>
		</div>
		<div class="col-md-6">
			<br>
			<br>
			<br>
			<br>
			<img src="<?php echo base_url(); ?>/assets/images/mision.jpg" alt="imagen4" width="100%"  height="200px">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			<br>
			<br>
			<br>
			<img src="<?php echo base_url(); ?>/assets/images/vision.jpg" alt="imagen4" width="100%"  height="200px">
		</div>
		<div class="col-md-6">
			<h2>VISIÓN</h2>
			<p>Promover el crecimiento sustentable de la organización, basado en la innovación permanente tanto en la prestación de los servicios, el acercamiento a los clientes, la calidad de la información brindada, el asesoramiento adecuado para cada necesidad.
				Lograr una organización flexible a los cambios que el mercado requiere, anticipar las tendencias para generar propuestas diferenciadoras y variadas.
				Fortalecer en forma constante la inversión en tecnología para el desarrollo de nuestra actividad.
				Posicionar a Hayland Travel como consolidador para toda la Patagonia.
				Generar acciones que estimulen las alianzas con organizaciones que colaboren socialmente en diversos ámbitos de la ciudad, a favor de la equidad social y el desarrollo deportivo.</p>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
</div>
