<br>
<div class="text-center" style="background-color: #85C1E9; color: black;">
  <div class="row">
    <div class="col-md-3">
      <p>40 años Cidando tus viajes</p>
      <p>&copy; 2023 Todos los derechos reservados.</p>
    </div>
    <div class="col-md-3">
      <p>Contactos</p>
      <p>informaciondevuelos@viajes.ec</p>
      <p>0989743456</p>
    </div>
    <div class="col-md-3">
      <p><a href="<?php echo site_url() ?>/aeropuertos/listar">Mi Aeropuerto</a></p>
      <p><a href="<?php echo site_url() ?>/pilotos/listar">Mi Piloto</a></p>
      <p><a href="<?php echo site_url() ?>/aviones/listar">Mi Viaje</a></p>
    </div>
    <div class="col-md-3">
      <img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo_agencia" style="width: 40%; height: 120px;">
    </div>
  </div>
</div>

</body>
</html>
