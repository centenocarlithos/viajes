<div class="container" style="background-color:#566573; color:black;">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>REGISTRO AVIONES</h1>
    </div>
  </div>
</div>
<br>
<div class="container">
  <form class="" action="<?php echo site_url(); ?>/aviones/guardar" method="post">
      <div class="row">
        <div class="col-md-6">
            <label for="">Código:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el código del avión"
            class="form-control"
            name="codigo_av" value="" id="codigo_av">
        </div>
        <div class="col-md-6">
            <label for="">Modelo:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el modelo del avión "
            class="form-control"
            name="modelo_av" value="" id="modelo_av">
        </div>
        <div class="col-md-6">
          <label for="">Origen:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el origen de partida del avión"
          class="form-control"
          name="origen_av" value="" id="origen_av">
        </div>
        <div class="col-md-6">
            <label for="">Destino:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el destino del avión"
            class="form-control"
            name="destino_av" value="" id="destino_av">
        </div>
        <div class="col-md-6">
            <label for="">Capacidad:</label>
            <br>
            <input type="number"
            placeholder="Ingrese la capacidad de pasajeros en el avión"
            class="form-control"
            name="capacidad_av" value="" id="capacidad_av">
        </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-12 text-center">
              <button type="submit" name="button"
              class="btn btn-primary">
                Guardar
              </button>
              &nbsp;
              <a href="<?php echo site_url(); ?>/aviones/listar" class="btn btn-danger">
                Cancelar
              </a>
          </div>
      </div>
  </form>
</div>
