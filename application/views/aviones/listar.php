<div class="container" style="background-color:#096B88; color:black;">
  <div class="row">
    <div class="col-md-8">
      <h1>LISTADO DE AVIONES: </h1>
    </div>
    <div class="col-md-4">
      <br>
      <a href="<?php echo site_url('aviones/registrar'); ?>" class="btn btn-warning">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Aeropuerto
      </a>
    </div>
  </div>
</div>
<br>
<!-- ifelse y tabulador -->
<?php if ($aviones): ?>
  <div class="container">
    <div class="row">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>CÓDIGO</th>
            <th>MODELO</th>
            <th>ORIGEN</th>
            <th>DESTINO</th>
            <th>CAPACIDAD</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($aviones as $filatemporal): ?>
            <tr>
              <td>
                <?php echo $filatemporal->id_av?>
              </td>
              <td>
                <?php echo $filatemporal->codigo_av?>
              </td>
              <td>
                <?php echo $filatemporal->modelo_av?>
              </td>
              <td>
                <?php echo $filatemporal->origen_av?>
              </td>
              <td>
                <?php echo $filatemporal->destino_av?>
              </td>
              <td>
                <p><?php echo $filatemporal->capacidad_av?> pasajeros</p>
              </td>
              <td class="text-center">
                <a href="#" title="Editar Avión" style="color:blue;"><i class="glyphicon glyphicon-pencil"></i></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/Aviones/eliminar/<?php echo $filatemporal->id_av?>" title="Eliminar Avión" onclick="return confirm('¿Estás seguro de eliminar de forma permanente el registro seleccionado?');" style="color:red;"><i class="glyphicon glyphicon-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
<?php else: ?>
  <div class="container" style="background-color:red; color:black;">
    <div class="row">
      <div class="col-md-12 text-center">
        <h3>NO EXISTEN DATOS EN EL LISTADO DE AVIONES</h3>
        <img src="<?php echo base_url(); ?>/assets/images/datos.jpg" alt="imagen_no_hay_datos" width="40%" height="300px">
        <h3>Ingrese datos para poder visualizar en el listado...</h3>
      </div>
    </div>
  </div><?php endif; ?>
