<div class="container" style="background-color:#26F652; color:black;">
  <div class="row">
    <div class="col-md-8">
      <h1>LISTADO DE AEROPUERTOS: </h1>
    </div>
    <div class="col-md-4">
      <br>
      <a href="<?php echo site_url('aeropuertos/registrar'); ?>" class="btn btn-info">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Aeropuerto
      </a>
    </div>
  </div>
</div>
<br>
<!-- ifelse y tabulador -->
<?php if ($aeropuertos): ?>
  <div class="container">
    <div class="row">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>CÓDIGO</th>
            <th>CIUDAD</th>
            <th>NOMBRE</th>
            <th>TELÉFONO</th>
            <th>CONTINENTE</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($aeropuertos as $filatemporal): ?>
            <tr>
              <td>
                <?php echo $filatemporal->id_ar?>
              </td>
              <td>
                <?php echo $filatemporal->code_ar?>
              </td>
              <td>
                <?php echo $filatemporal->ciudad_ar?>
              </td>
              <td>
                <?php echo $filatemporal->nombre_ar?>
              </td>
              <td>
                <?php echo $filatemporal->telefono_ar?>
              </td>
              <td>
                <?php echo $filatemporal->continente_ar?>
              </td>
              <td class="text-center">
                <a href="#" title="Editar Aeropuerto" style="color:blue;"><i class="glyphicon glyphicon-pencil"></i></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/Aeropuertos/eliminar/<?php echo $filatemporal->id_ar?>" title="Eliminar Aeropuerto" onclick="return confirm('¿Estás seguro de eliminar de forma permanente el registro seleccionado?');"  style="color:red;"><i class="glyphicon glyphicon-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
<?php else: ?>
  <div class="container" style="background-color:red; color:black;">
    <div class="row">
      <div class="col-md-12 text-center">
        <h3>NO EXISTEN DATOS EN EL LISTADO DE AEROPUERTOS</h3>
        <img src="<?php echo base_url(); ?>/assets/images/datos.jpg" alt="imagen_no_hay_datos" width="40%" height="300px">
        <h3>Ingrese datos para poder visualizar en el listado...</h3>
      </div>
    </div>
  </div><?php endif; ?>
