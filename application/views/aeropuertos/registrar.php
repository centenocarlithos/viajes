<div class="container" style="background-color:#566573; color:black;">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>REGISTRO AEROPUERTOS</h1>
    </div>
  </div>
</div>
<br>
<div class="container">
  <form class="" action="<?php echo site_url(); ?>/aeropuertos/guardar" method="post">
      <div class="row">
        <div class="col-md-6">
            <label for="">Código:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el código del aeropuerto"
            class="form-control"
            name="code_ar" value="" id="code_ar">
        </div>
        <div class="col-md-6">
            <label for="">Ciudad:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la ciudad del aeropuerto "
            class="form-control"
            name="ciudad_ar" value="" id="ciudad_ar">
        </div>
        <div class="col-md-6">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del aeropuerto"
          class="form-control"
          name="nombre_ar" value="" id="nombre_ar">
        </div>
        <div class="col-md-6">
            <label for="">Teléfono:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el teléfono del aeropuerto"
            class="form-control"
            name="telefono_ar" value="" id="telefono_ar">
        </div>
        <div class="col-md-6">
            <label for="">Continente:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el continente del aeropuerto"
            class="form-control"
            name="continente_ar" value="" id="continente_ar">
        </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-12 text-center">
              <button type="submit" name="button"
              class="btn btn-primary">
                Guardar
              </button>
              &nbsp;
              <a href="<?php echo site_url(); ?>/pilotos/listar" class="btn btn-danger">
                Cancelar
              </a>
          </div>
      </div>
  </form>
</div>
