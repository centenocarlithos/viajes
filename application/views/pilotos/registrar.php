<div class="container" style="background-color:#566573; color:black;">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>REGISTRO PILOTOS</h1>
    </div>
  </div>
</div>
<br>
<div class="container">
  <form class="" action="<?php echo site_url(); ?>/pilotos/guardar" method="post">
      <div class="row">
        <div class="col-md-6">
            <label for="">Cédula:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la cédula del piloto"
            class="form-control"
            name="cedula_pi" value="" id="cedula_pi">
        </div>
        <div class="col-md-6">
            <label for="">Apellidos:</label>
            <br>
            <input type="text"
            placeholder="Ingrese los apellidos del piloto"
            class="form-control"
            name="apellidos_pi" value="" id="apellidos_pi">
        </div>
        <div class="col-md-6">
          <label for="">Nombres:</label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres del piloto"
          class="form-control"
          name="nombres_pi" value="" id="nombres_pi">
        </div>
        <div class="col-md-6">
            <label for="">Teléfono:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el teléfono del piloto"
            class="form-control"
            name="telefono_pi" value="" id="telefono_pi">
        </div>
        <div class="col-md-6">
            <label for="">Edad:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la edad del piloto"
            class="form-control"
            name="edad_pi" value="" id="edad_pi">
        </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-12 text-center">
              <button type="submit" name="button"
              class="btn btn-primary">
                Guardar
              </button>
              &nbsp;
              <a href="<?php echo site_url(); ?>/pilotos/listar" class="btn btn-danger">
                Cancelar
              </a>
          </div>
      </div>
  </form>
</div>
