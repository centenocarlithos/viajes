<div class="container" style="background-color:#F59112; color:black;">
  <div class="row">
    <div class="col-md-8">
      <h1>LISTADO DE PILOTOS: </h1>
    </div>
    <div class="col-md-4">
      <br>
      <a href="<?php echo site_url('pilotos/registrar'); ?>" class="btn btn-success">
        Agregar Piloto
        <i class="glyphicon glyphicon-plus"></i>
      </a>
    </div>
  </div>
</div>
<br>
<!-- ifelse y tabulador -->
<?php if ($pilotos): ?>
  <div class="container">
    <div class="row">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>CÉDULA</th>
            <th>APELLIDOS</th>
            <th>NOMBRES</th>
            <th>TELÉFONO</th>
            <th>EDAD</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($pilotos as $filatemporal): ?>
            <tr>
              <td>
                <?php echo $filatemporal->id_pi?>
              </td>
              <td>
                <?php echo $filatemporal->cedula_pi?>
              </td>
              <td>
                <?php echo $filatemporal->apellidos_pi?>
              </td>
              <td>
                <?php echo $filatemporal->nombres_pi?>
              </td>
              <td>
                <?php echo $filatemporal->telefono_pi?>
              </td>
              <td>
                <?php echo $filatemporal->edad_pi?>
              </td>
              <td class="text-center">
                <a href="#" title="Editar Piloto" style="color:blue;"><i class="glyphicon glyphicon-pencil"></i></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/Pilotos/eliminar/<?php echo $filatemporal->id_pi?>" title="Eliminar Piloto" onclick="return confirm('¿Estás seguro de eliminar de forma permanente el registro seleccionado?');" style="color:red;"><i class="glyphicon glyphicon-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- <?php print_r($pilotos); ?> -->
<?php else: ?>
  <div class="container" style="background-color:red; color:black;">
    <div class="row">
      <div class="col-md-12 text-center">
        <h3>NO EXISTEN DATOS EN EL LISTADO DE PILOTOS</h3>
        <img src="<?php echo base_url(); ?>/assets/images/datos.jpg" alt="imagen_no_hay_datos" width="40%" height="300px">
        <h3>Ingrese datos para poder visualizar en el listado...</h3>
      </div>
    </div>
  </div>
<?php endif; ?>
